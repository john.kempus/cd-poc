import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as ecs from "aws-cdk-lib/aws-ecs";
import * as ecs_patterns from "aws-cdk-lib/aws-ecs-patterns";
import * as ecr from "aws-cdk-lib/aws-ecr";

export class ECRStack extends cdk.Stack {
  readonly repo: ecr.Repository
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const containerRepositry = new ecr.Repository(this, 'NginxRegistryRepositoy', {
      repositoryName: "nginx"
    })
    
    new cdk.CfnOutput(this, 'RepoURL', {
      value: containerRepositry.repositoryUri,
      description: 'ECR Repository URL'
    });

    this.repo = containerRepositry
  }
}
