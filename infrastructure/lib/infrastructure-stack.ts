import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as ecs from "aws-cdk-lib/aws-ecs";
import * as ecs_patterns from "aws-cdk-lib/aws-ecs-patterns";
import * as ecr from "aws-cdk-lib/aws-ecr";

export class InfrastructureStack extends cdk.Stack {

  readonly containerRegistryRepository: ecr.Repository
  constructor(scope: Construct, id: string, props?: cdk.StackProps, containerRegistryRepository?: ecr.Repository) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, "MyVpc", {
      maxAzs: 3, // Default is all AZs in region
      vpcName: "MyVpc"
    });

    const cluster = new ecs.Cluster(this, "MyCluster", {
      vpc: vpc,
      clusterName: "MyCluster"
    });

    // Create a load-balanced Fargate service and make it public
    const myService = new ecs_patterns.ApplicationLoadBalancedFargateService(this, "NginxService", {
      cluster: cluster, // Required
      cpu: 256, // Default is 256
      desiredCount: 1, // Default is 1
      taskImageOptions: { family: "nginx-task-definition", image: ecs.ContainerImage.fromRegistry("public.ecr.aws/nginx/nginx") },
      memoryLimitMiB: 512, // Default is 512
      publicLoadBalancer: true, // Default is true,
      serviceName: "NginxService",
      loadBalancerName: "nginx",
    });

    if (myService.taskDefinition.executionRole && containerRegistryRepository) {
      containerRegistryRepository.grantPull(myService.taskDefinition.executionRole)
    }
  }
}
