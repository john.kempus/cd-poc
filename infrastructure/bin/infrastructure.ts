#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { InfrastructureStack } from '../lib/infrastructure-stack';
import { ECRStack } from '../lib/ecr-stack';

const app = new cdk.App();


const repoStack = new ECRStack(app, 'EcrStack', {
  env: { account: app.account, region: "ap-southeast-1" },
})

const stgInfra = new InfrastructureStack(app, 'StagingStack', {
  env: { account: app.account, region: "ap-southeast-1" },
}, repoStack.repo);

stgInfra.addDependency(repoStack)

const prodInfra = new InfrastructureStack(app, 'ProdStack', {
  env: { account: app.account, region: "us-west-1" },
}, repoStack.repo);

prodInfra.addDependency(repoStack)
