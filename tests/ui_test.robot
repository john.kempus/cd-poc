*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${URL}  http://nginx-832835034.ap-southeast-1.elb.amazonaws.com  # Replace with the URL you're testing
${BROWSER}  chrome

*** Test Cases ***
Check Web Page is Accessible
    Open Browser To Page
    Capture Page Screenshot

*** Keywords ***
Open Browser To Page
    Open Browser  ${URL}  ${BROWSER}

Validate Page Content
    ${body_text}=  Get Text  tag=body
    Should Be Equal  ${body_text}  Hello John
